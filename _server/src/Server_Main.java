import java.net.*;
import java.io.*;
public class Server_Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ServerSocket serverForClient = null;
		Socket socketOnServer = null;
		ServerThread thread;
		while(true){
			try{
				serverForClient = new ServerSocket(2010);
			}
			catch(IOException el){
				System.out.println("正在监听");
			}
			try{
				System.out.println("等待客户呼叫");
				socketOnServer = serverForClient.accept();
				System.out.println("客户的地址："+serverForClient.getInetAddress());
			}
			catch(IOException e){
				System.out.println("正在等待客户");
			}
			if(serverForClient!=null){
				new ServerThread(socketOnServer).start();
			}
		}
	}
}
class ServerThread extends Thread{
			Socket socket;
			DataOutputStream out =null;
			DataInputStream in = null;
			ServerThread(Socket t){
				socket =t;
				try{
					out = new DataOutputStream(socket.getOutputStream());
					in = new DataInputStream(socket.getInputStream());
				}
				catch(IOException e){}
			}
			public void run(){
				while(true){
					try{
						String s = in.readUTF();
						System.out.println("服务器收到客户的提问"+s);
						out.writeUTF("回应");
						Thread.sleep(500);
					}
					catch(Exception e){
						System.out.println("客户已断开"+e);
					}
				}
			}
		}
	
